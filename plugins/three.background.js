
import * as THREE from 'three'
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader.js'

// Container Select
const parentcont = document.getElementById('card-gallery')
const mycont = document.getElementById('threejs')
const mouse = new THREE.Vector2()
let mouseX = 0
let mouseY = 0
const mouseDelta = 0
const target = new THREE.Vector3()
let windowHalfX = parentcont.clientWidth / 2
let windowHalfY = parentcont.clientHeight / 2

// Init Scene
const loader = new GLTFLoader()
const scene = new THREE.Scene()
const light = new THREE.PointLight(0xFFFFF0, 1, 100)
light.position.set(2, 2, 2)
scene.add(light)
const renderer = new THREE.WebGLRenderer({ canvas: mycont, antialias: true })
renderer.setPixelRatio(window.devicePixelRatio)
renderer.setSize(parentcont.clientWidth, parentcont.clientHeight)
const camera = new THREE.PerspectiveCamera(75, mycont.clientWidth / mycont.clientHeight, 0.1, 1000)

let model = null

function init () {
  scene.background = new THREE.Color(0xCB4A60)
  // Load a glTF resource
  loader.load(
    // resource URL
    'models/flower1.glb',
    // called when the resource is loaded
    function (gltf) {
      model = gltf.scene
      initScene()
    },
    // called while loading is progressing
    function (xhr) {
      console.log((xhr.loaded / xhr.total * 100) + '% loaded')
    },
    // called when loading has errors
    function (error) {
      console.log('An error happened', error)
    }
  )
  window.addEventListener('resize', onWindowResize, false)
}

function animate () {
  if (model) {
    model.rotation.y += 0.003
    target.x = (1 - mouseX) * 0.0001
    target.y = (1 - mouseY) * 0.0001
    target.z = 1 - mouseDelta
    camera.position.x += 1 * (target.y - camera.position.x)
    camera.rotation.x += 1 * (target.y - camera.rotation.x)
    camera.rotation.y += 1 * (target.x - camera.rotation.y)
    // camera.position.y += 1 * (target.z - camera.position.y)
  }

  requestAnimationFrame(animate)
  renderer.render(scene, camera)
}

function initScene () {
  scene.add(model)
  camera.position.x = 0
  camera.position.y = 0.9
  camera.position.z = 0.6
  camera.lookAt(0, 1, 0)
}

// Helpers
function onWindowResize () {
  camera.aspect = parentcont.clientWidth / parentcont.clientHeight
  camera.updateProjectionMatrix()
  renderer.setSize(parentcont.clientWidth, parentcont.clientHeight)
  windowHalfX = parentcont.clientWidth / 2
  windowHalfY = parentcont.clientHeight / 2
}

function onDocumentMouseMove (event) {
  mouseX = event.clientX - windowHalfX
  mouseY = event.clientY - windowHalfY
  mouse.x = (event.clientX / window.innerWidth) * 2 - 1
  mouse.y = -(event.clientY / window.innerHeight) * 2 + 1
}

// MoveChanger
if (window.DeviceOrientationEvent && isMobileDevice()) {
  window.addEventListener('deviceorientation', function (ev) {
    mouseX = ev.gamma * 100
    mouseY = -ev.beta * 80
  })
} else {
  document.addEventListener('mousemove', onDocumentMouseMove, false)
}
function isMobileDevice () {
  return (typeof window.orientation !== 'undefined') || (navigator.userAgent.includes('IEMobile'))
};

// Execution
init()
animate()
