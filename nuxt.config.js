export default {
  // Target: https://go.nuxtjs.dev/config-target
  server: {
    host: '0' // default: localhost
  },
  ssr: false,
  target: 'static',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'World of Woodles Collection',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' },
      { hid: 'description', name: 'description', content: 'World of Woodles NFT Collection 2022' },
      { name: 'theme-color', content: '#911845' }
    ],
    link: [
      { rel: 'apple-touch-icon', href: '/icon.png?v1', type: 'image/png' },
      { rel: 'icon', href: '/icon.png?v1', type: 'image/png' },
      { rel: 'shortcut icon', href: '/icon.png?v1', type: 'image/png' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    'normalize.css',
    'vuesax/dist/vuesax.css',
    'boxicons/css/boxicons.css',
    '~/assets/styles/app.scss'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: ['~/plugins/vuesax', { src: '~/plugins/progress', mode: 'client' }, { src: '~/plugins/vue-carousel', mode: 'client' }],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    '@nuxt/http'
  ],

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: 'en'
    }
  },
  build: {
    postcss: {
      preset: {
        autoprefixer: true
      }
    }
  }
}
